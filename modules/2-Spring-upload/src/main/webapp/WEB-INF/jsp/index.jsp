<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="true" %>
<!DOCTYPE html>

<html lang="en">
    <head>
        <noscript><meta http-equiv="refresh" content="0; url=wrongplace.htm" /></noscript>
        <meta charset="utf-8" />
        <link rel="stylesheet" type="text/css" href="css/index.css">
        <link rel="icon" 
              type="image/png" 
              href="media/img/favicon.png">
        
        <script src="js/jquery-1.11.2.min.js"></script>
        <script src="js/rightclick.min.js"></script>
        <script src="js/getDate.min.js"></script>
        <script src="js/controller/index.min.js"></script>
        
        <title>Welcome to Parties 2015 General Election</title>
    </head>
    <body>
        <c:set var="status" scope="session" value="${status}"/>
        <c:set var="sms" scope="session" value="${sms}"/>
        
        <h1>Welcome to Parties <script>document.write(year);</script> General Election</h1>
        <img src='media/img/favicon.png' alt='election'/>
        <c:if test="${status == 'start' or status == 'dui' or status == 'noUser'}">
            <form action="index.htm" id="formDUI" method="post">
                <h2>Please, insert your Document ID to continue:</h2>
                <input type="text" name="txtDUI" placeholder="Document ID" id="dui" maxlength="10" autocomplete='off' />
                <br />
                <label id="lblErr">
                    <c:if test="${status == 'dui'}">
                        Invalid!
                    </c:if>
                    <c:if test="${status == 'noUser'}">
                        The Document ID doesn't exist.
                    </c:if>
                </label>
                <br />
                <input type="submit" id="btnEnter" value="Enter" name="submit" disabled="disabled" />
            </form>
        </c:if>
        
        <c:if test="${status == 'sent' or status == 'wrongSMS'}">
            <form id="formSMS" action="index.htm" method="post">
                <c:out value="${sms}" />
                <h1>Please, insert your SMS:</h1>
                <input type="text" name="txtSMS" placeholder='sms' id="sms" maxlength="4" autocomplete='off' />
                <input type="submit" id="btnSMS" value="Enter" name="btnSMS" disabled="disabled" />
                <label class='lblError'>
                    <c:if test="${status == 'wrongSMS'}">
                        Wrong SMS!
                    </c:if>
                </label>
                <p>A SMS has been to your cell phone, please enter in the text box above, it's valid for 15 minutes.</p>
            </form>
        </c:if>
        <img src="media/img/loading.gif" alt="loading" style='display: none;' id="imgLoading" />
    </body>
</html>