package pl.dmcs.wpft.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * @author Robert Ritter
 */
@Controller
@RequestMapping(value = "/upload")
public class UploadController {
    private Logger log = LoggerFactory.getLogger(UploadController.class);

    @RequestMapping("/test.spring")
    public
    @ResponseBody
    String test() {
        return "I am a working controller";
    }

    @RequestMapping(value = "/file.spring", method = RequestMethod.POST)
    public String handleFormUpload(@RequestParam("fileInput") MultipartFile file) throws IOException {

        if (!file.isEmpty()) {
            log.info("just uploaded '{}'", file.getOriginalFilename());
            byte[] bytes = file.getBytes();
            // store the bytes somewhere
            return "redirect:uploadSuccess";
        }

        return "redirect:uploadFailure";
    }

}
