/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.dmcs.wpft.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Random;

/**
 * Changed by Robert Ritter
 *
 * @author Federico
 */
@Controller
public class IndexController {

    private Logger log = LoggerFactory.getLogger(IndexController.class);

    @RequestMapping(value = "/", method = {RequestMethod.GET})
    public String get(HttpServletRequest hsr, HttpServletResponse hsr1, Model model) {
        log.info("not authenticated");
        model.addAttribute("john", "doe");

        boolean test = new Random().nextBoolean();

        if (test)
            return "authenticated";
        else return "not_authenticated";
    }

    @RequestMapping(value = "/", method = {RequestMethod.POST})
    public
    @ResponseBody
    String post(HttpServletRequest hsr, HttpServletResponse hsr1, @RequestParam(required = false, defaultValue = "") String txtDUI) throws Exception {
        log.info("Index Controller just hit");

        log.info("request came from: {}", hsr.getHeader("User-Agent"));

        log.info("txtDUI: {}", txtDUI);

        boolean test = new Random().nextBoolean();

        if (test)
            return "authenticated";
        else return "not_authenticated";

//        ModelAndView mv = new ModelAndView("index");

//        int temp = VotingPeriodDAO.checkPeriod();
//        String contextPath;

//        if (temp > 0) {
//            if (hsr.getParameter("submit") != null) {
//
//                String dui = hsr.getParameter("txtDUI");
//
//                String pattern = "^\\d{8}-\\d{1}$";
//
//                if (dui.matches(pattern)) {
//
////                    List<VotingPopulation> user = VotingPopulationDAO.getUser(dui);
//
////                    if (!user.isEmpty()) {
////                        List<VotingType> tempUser = VotingTypeDAO.checkCitizen(dui);
////
////                        if (!tempUser.isEmpty()) {
////                            Random rand = new Random();
////
////                            // nextInt is normally exclusive of the top value,
////                            // so add 1 to make it inclusive
////                            int randomNum = rand.nextInt((9999 - 1) + 1) + 1;
////
////                            String number = String.format("%04d", randomNum);
////
////                            mv.addObject("sms", number);
////                            mv.addObject("status", "sent");
////
////                            //The limit is 15 minutes
////                            hsr.getSession().setMaxInactiveInterval(900);
////                            hsr.getSession().setAttribute("SMS", number);
////                            hsr.getSession().setAttribute("User", dui);
////                        } else {
////                            contextPath = "/VoteNow/already.htm";
////                            hsr1.sendRedirect(hsr1.encodeRedirectURL(contextPath));
////                        }
//                    } else {
//                        mv.addObject("status", "noUser");
//                    }
//                } else {
//                    mv.addObject("status", "dui");
//                }
//            } else if (hsr.getParameter("btnSMS") != null) {
//
//                if (hsr.getSession().getAttribute("SMS").toString().equals(hsr.getParameter("txtSMS"))) {
//                    contextPath = "/VoteNow/electionType.htm";
//                    hsr1.sendRedirect(hsr1.encodeRedirectURL(contextPath));
//                } else {
//                    mv.addObject("status", "wrongSMS");
//                }
//            } else {
//                mv.addObject("status", "start");
//            }
//        } else {
//            try {
//                hsr.getSession().removeAttribute("SMS");
//                hsr.getSession().removeAttribute("User");
//            } catch (Exception ex) {
//            }
//            contextPath = "/VoteNow/wrongplace.htm?session=invalid";
//            hsr1.sendRedirect(hsr1.encodeRedirectURL(contextPath));
//        }
//        return mv;
//        return "migrated";
    }
}
